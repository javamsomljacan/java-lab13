package org.vub.lab13;
import org.w3c.dom.ls.LSOutput;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Stream<String> s = Stream.of("Ponedjeljak","Utorak","Srijeda","Cetvrtak","Petak","Subota","Nedjelja");

        List<String> lista = s.collect(Collectors.toList());

        //Zad1
        lista.stream().filter(l -> l.endsWith("k")).forEach(System.out::println);

        //Zad2
        lista.stream().map(n -> n.substring(0,3)).forEachOrdered(n -> System.out.println(n));

        //Zad3
        lista.stream().filter(e -> (e.length()>8)).forEachOrdered(e -> System.out.println(e));

    }
}
